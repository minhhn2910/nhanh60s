	var center = function (modal) {
					var top, left;

					top = Math.max($(window).height() - $(modal).outerHeight(), 0) / 2;
					left = Math.max($(window).width() - $(modal).outerWidth(), 0) / 2;

					$(modal).css({
						top:top + $(window).scrollTop(), 
						left:left + $(window).scrollLeft()
					});
				};
	var open_form = function (modal) {
		//			$content.empty().append(settings.content);

					$(modal).css({
						width: 'auto', 
						height: 'auto'
					});

					center(modal);
					$(window).bind('resize.modal', center());
					$(modal).show();
					$('#overlay').show();
				};

				// Close the modal
	var close_form = function () {
					$('#modal-login').hide();
					$('#modal-register').hide();
					$('#overlay').hide();
					$(window).unbind('resize.modal');
				};
		// Wait until the DOM has loaded before querying the document
	$(document).ready(function(){
		
			$('a#login-action').click(function(e){
				
				open_form('#modal-login');
				e.preventDefault();
				});		

			$('a#register-action').click(function(e){
				open_form('#modal-register');
				e.preventDefault();
				});		
			$('#overlay').click(function(e){
				close_form();
				e.preventDefault();
				});				
		});
			
