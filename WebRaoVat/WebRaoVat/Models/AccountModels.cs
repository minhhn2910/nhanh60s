﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace WebRaoVat.Models
{
    /*
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("name=raovat_dbEntities")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }
    */
    [Table("UserProfile")]
    public partial class UserProfile
    {
   /*     [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }*/
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class UserEditModel
    {
        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }

        [Display(Name = "Loại User")]
        public string UserType { get; set; }

        [Display(Name = "Hình Banner")]
        public string BannerImage { get; set; }

        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Display(Name = "Điện thoại")]
        public string Phone { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Yahoo")]
        public string Yahoo { get; set; }

        [Display(Name = "Skype")]
        public string Skype { get; set; }

        [Display(Name = "HotLine")]
        public string HotLine { get; set; }

        [Display(Name = "Slogan")]
        public string Slogan { get; set; }

        [Display(Name = "Hình logo thu nhỏ")]
        public string LogoImage { get; set; }

        [Display(Name = "Link quảng cáo")]
        public string CustomAds { get; set; }

    }
    public class ResetPasswordModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

    }

    public class ResetPasswordConfirmModel
    {

        public string Token { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "2 password đã nhập không giống nhau")]
        public string ConfirmPassword { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }
     
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Điện thoại")]
        public string Phone { get; set; }



    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
