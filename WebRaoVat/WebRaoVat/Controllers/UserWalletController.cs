﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class UserWalletController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /UserWallet/

        public ActionResult Index(string Search)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var userwallets = db.UserWallets.Include(u => u.UserProfile);
                if (!String.IsNullOrEmpty(Search))
                    userwallets = userwallets.Where(a => a.UserProfile.UserName.Contains(Search));
                ViewBag.searchstring = Search;
                return View(userwallets.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserWallet/Details/5

        public ActionResult Details(int id = 0)
        {
            UserWallet userwallet = db.UserWallets.Find(id);
            if (userwallet == null || userwallet.UserId != WebSecurity.CurrentUserId)
            {
                return HttpNotFound();
            }
            return View(userwallet);
        }

        //
        // GET: /UserWallet/Create

        public ActionResult Create()
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName");
                return View();
            }
            else return HttpNotFound();
        }

        //
        // POST: /UserWallet/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserWallet userwallet)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                if (ModelState.IsValid)
                {
                    db.UserWallets.Add(userwallet);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", userwallet.UserId);
                return View(userwallet);
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserWallet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                UserWallet userwallet = db.UserWallets.Find(id);
                if (userwallet == null)
                {
                    return HttpNotFound();
                }
                ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", userwallet.UserId);
                return View(userwallet);
            }
            else return HttpNotFound();
        }

        //
        // POST: /UserWallet/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserWallet userwallet)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(userwallet).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", userwallet.UserId);
                return View(userwallet);
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserWallet/Delete/5
/*
        public ActionResult Delete(int id = 0)
        {
            UserWallet userwallet = db.UserWallets.Find(id);
            if (userwallet == null)
            {
                return HttpNotFound();
            }
            return View(userwallet);
        }

        //
        // POST: /UserWallet/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserWallet userwallet = db.UserWallets.Find(id);
            db.UserWallets.Remove(userwallet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
*/
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}