﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]    
    public class AdministratorDashboardController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();
        //
        // GET: /AdministratorDashboard/

        public ActionResult Index()
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) !=null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                ViewBag.AdminName = db.UserProfiles.Find(WebSecurity.CurrentUserId).FullName;
                return View();
            }
            else return HttpNotFound();
        }

    }
}
