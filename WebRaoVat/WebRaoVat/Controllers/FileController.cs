﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using WebRaoVat.Models;
namespace WebRaoVat.Controllers
{
    [Authorize]
    public class FileController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();
        //
        // GET: /File/
        
        [HttpPost]
       public string Upload()
        {
            List<string> file_list = new List<string>();
            foreach (string inputTagName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[inputTagName];
                if (file.ContentLength > 0)
                {
                       Guid g = Guid.NewGuid();
                      string GuidString = Convert.ToBase64String(g.ToByteArray());
                      GuidString = GuidString.Replace("=","");
                      GuidString = GuidString.Replace("+","");
                      GuidString = GuidString.Replace("/", "");
                      GuidString = GuidString.Replace("\\", "");
                      string old_file_extension = Path.GetExtension(Path.GetFileName(file.FileName));
                       string file_name = GuidString + old_file_extension;
                      file_list.Add(file_name);
                       string filePath = Path.Combine(HttpContext.Server.MapPath("~/Content/Upload/Images"), file_name);
                    file.SaveAs(filePath);
                }
            }

            string[][] FileNames = file_list.Select(x => new string[]{x}).ToArray();
            string json_result = JsonConvert.SerializeObject(FileNames);
            return json_result;
        }
        [HttpPost]
        public string Delete(string filename)
        {
            if (filename == "NoImage.jpg")
                return "OK";
            string temp2 = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Upload/Images");
            string temp = HttpContext.Server.MapPath("~/Content/Upload/Images");
            string fullPath = Path.Combine(temp, filename);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
                try
                {
                    PostImage post_image = db.PostImages.Where(x => x.ImageURL == filename).Single<PostImage>();
                    if (post_image != null)
                    {
                        db.PostImages.Remove(post_image);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                { return "ERROR"; }
                return "OK";
            }
            else
                return "Error";

        }

        public string Delete1(string filename)
        {
            if (filename == null)
                return "Error";
            if (filename == "NoImage.jpg")
                return "OK";
            string fullPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Upload/Images"), filename);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
                try
                {
                    PostImage post_image = db.PostImages.Where(x => x.ImageURL == filename).Single<PostImage>();
                    if (post_image != null)
                    {
                        db.PostImages.Remove(post_image);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                { return "ERROR"; }
                return "OK";
            }
            else
                return "Error";

        }
    }
}
