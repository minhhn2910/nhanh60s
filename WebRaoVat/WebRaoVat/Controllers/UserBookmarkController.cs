﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;

namespace WebRaoVat.Controllers
{
    public class UserBookmarkController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /UserBookmark/

        public ActionResult Index(string Search)
        {
            var bookmarks = db.Bookmarks.Where(a => a.UserId == WebSecurity.CurrentUserId).Include(b => b.Post).Include(b => b.UserProfile);
            if (!String.IsNullOrEmpty(Search))
                bookmarks = bookmarks.Where(a => a.UserComment.Contains(Search));
            ViewBag.searchstring = Search;
            return View(bookmarks.ToList());
        }

        //
        // GET: /UserBookmark/Details/5

        public ActionResult Details(int id = 0)
        {
            Bookmark bookmark = db.Bookmarks.Find(id);
            if (bookmark == null)
            {
                return HttpNotFound();
            }
            return View(bookmark);
        }

        //
        // GET: /UserBookmark/Create

/*        public ActionResult Create()
        {
            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Title");
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }
*/
        //
        // POST: /UserBookmark/Create

/*        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Bookmark bookmark)
        {
            if (ModelState.IsValid)
            {
                db.Bookmarks.Add(bookmark);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Title", bookmark.PostId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", bookmark.UserId);
            return View(bookmark);
        }
*/
        //
        // Get: /UserBookmark/Create/id

        public ActionResult Create(int id = 0)
        {
            Bookmark bookmark;
            bookmark = new Bookmark();
            ViewBag.PostId = id;
            if (WebSecurity.CurrentUserId < 0)
                return View();

            if (db.Posts.Find(id) != null)
            {
                bookmark.PostId = id;
                bookmark.UserComment = db.Posts.Find(id).Title;
                bookmark.UserId = WebSecurity.CurrentUserId;
                db.Bookmarks.Add(bookmark);
                db.SaveChanges();
                return RedirectToAction("Details", "Post", new { id = id });
            }

            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Title", bookmark.PostId);
            ViewBag.UserId = new SelectList(db.UserProfiles.Where(b => b.UserId == WebSecurity.CurrentUserId), "UserId", "UserName", bookmark.UserId);
            return View(bookmark);
        }

        //
        // GET: /UserBookmark/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Bookmark bookmark = db.Bookmarks.Find(id);
            if (WebSecurity.CurrentUserId != bookmark.UserId)
                return HttpNotFound();
            if (bookmark == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Title", bookmark.PostId);
            ViewBag.UserId = new SelectList(db.UserProfiles.Where(u => u.UserId == WebSecurity.CurrentUserId), "UserId", "UserName", bookmark.UserId);
            return View(bookmark);
        }

        //
        // POST: /UserBookmark/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Bookmark bookmark)
        {
            if (WebSecurity.CurrentUserId != bookmark.UserId)
                return HttpNotFound();
            if (ModelState.IsValid)
            {
                db.Entry(bookmark).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Title", bookmark.PostId);
            ViewBag.UserId = new SelectList(db.UserProfiles.Where(u => u.UserId == WebSecurity.CurrentUserId), "UserId", "UserName", bookmark.UserId);
            return View(bookmark);
        }

        //
        // GET: /UserBookmark/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Bookmark bookmark = db.Bookmarks.Find(id);
            if (bookmark == null || (WebSecurity.CurrentUserId != bookmark.UserId))
            {
                return HttpNotFound();
            }
            return View(bookmark);
        }

        //
        // POST: /UserBookmark/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bookmark bookmark = db.Bookmarks.Find(id);
            if (WebSecurity.CurrentUserId != bookmark.UserId)
                return HttpNotFound();
            db.Bookmarks.Remove(bookmark);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}