﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class CategoryAdController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /CategoryAd/

        public ActionResult Index(string Search, string Category, string IsVertical, string Expired)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var CategoryLst = new List<string>();
                var IsVerticalLst = new List<string>();
                var ExpiredLst = new List<string>();

                var CategoryQry = from d in db.PostCategories
                               select d.CategoryName;

                IsVerticalLst.Add("All");
                IsVerticalLst.Add("Ngang");
                IsVerticalLst.Add("Dọc");

                ExpiredLst.Add("All");
                ExpiredLst.Add("Hết Hạn");
                ExpiredLst.Add("Còn Hạn");

                CategoryLst.Add("All");
                CategoryLst.AddRange(CategoryQry.Distinct());
                
                ViewBag.Category = new SelectList(CategoryLst, Category);
                ViewBag.IsVertical = new SelectList(IsVerticalLst, IsVertical);
                ViewBag.Expired = new SelectList(ExpiredLst, Expired);

                var categoryads = db.CategoryAds.Include(c => c.PostCategory);;

                if (!String.IsNullOrEmpty(Search))
                    categoryads = categoryads.Where(a => a.ImageURL.Contains(Search) 
                                                      || a.ImageLink.Contains(Search)
                                                   );
                if (!String.IsNullOrEmpty(Category) && Category != "All")
                    categoryads = categoryads.Where(a => a.PostCategory.CategoryName == Category);

                if (!String.IsNullOrEmpty(Category) && IsVertical != "All")
                {
                        categoryads = categoryads.Where(a => a.IsVertical == ((IsVertical == "Ngang") ? true : false));
                }

                if (!String.IsNullOrEmpty(Category) && Expired != "All")
                {
                    if (IsVertical == "Còn Hạn")
                        categoryads = categoryads.Where(a => a.ExpireDate.Value < DateTime.Now);
                    else
                        categoryads = categoryads.Where(a => a.ExpireDate.Value > DateTime.Now);
                }
 
                ViewBag.searchstring = Search;
                return View(categoryads.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /CategoryAd/Details/5

        public ActionResult Details(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                CategoryAd categoryad = db.CategoryAds.Find(id);
                if (categoryad == null)
                {
                    return HttpNotFound();
                }
                return View(categoryad);
            }
            else return HttpNotFound();
        }

        //
        // GET: /CategoryAd/Create

        public ActionResult Create()
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName");
                return View();
            }
            else return HttpNotFound();
        }

        //
        // POST: /CategoryAd/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryAd categoryad)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                string[] images_links = null;
                if (Request["images"] != null)
                    images_links = Request["images"].ToString().Split(':');
 //               if (ModelState.IsValid)
//                {
                    if (images_links.Length == 1)
                    {
                        if (!String.IsNullOrEmpty(images_links[0]))
                            categoryad.ImageURL = images_links[0];
                    }
                    db.CategoryAds.Add(categoryad);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                //}

               // ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", categoryad.CategoryId);
                //return View(categoryad);
            }
            else return HttpNotFound();
        }

        //
        // GET: /CategoryAd/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                CategoryAd categoryad = db.CategoryAds.Find(id);
                if (categoryad == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", categoryad.CategoryId);
                return View(categoryad);
            }
            else return HttpNotFound();
        }

        //
        // POST: /CategoryAd/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryAd categoryad)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(categoryad).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", categoryad.CategoryId);
                return View(categoryad);
            }
            else return HttpNotFound();
        }

        //
        // GET: /CategoryAd/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                CategoryAd categoryad = db.CategoryAds.Find(id);
                if (categoryad == null)
                {
                    return HttpNotFound();
                }
                return View(categoryad);
            }
            else return HttpNotFound();
        }

        //
        // POST: /CategoryAd/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                CategoryAd categoryad = db.CategoryAds.Find(id);
                FileController a = new FileController();
                a.Delete1(categoryad.ImageURL);
                db.CategoryAds.Remove(categoryad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}