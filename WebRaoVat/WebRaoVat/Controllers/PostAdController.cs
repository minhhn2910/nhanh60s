﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class PostAdController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /PostAd/

        public ActionResult Index(string Search, string Category, string IsVertical, string Expired)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var CategoryLst = new List<string>();
                var IsVerticalLst = new List<string>();
                var ExpiredLst = new List<string>();

                var CategoryQry = from d in db.PostCategories
                                  select d.CategoryName;

                IsVerticalLst.Add("All");
                IsVerticalLst.Add("Ngang");
                IsVerticalLst.Add("Dọc");

                ExpiredLst.Add("All");
                ExpiredLst.Add("Hết Hạn");
                ExpiredLst.Add("Còn Hạn");

                CategoryLst.Add("All");
                CategoryLst.AddRange(CategoryQry.Distinct());

                ViewBag.Category = new SelectList(CategoryLst, Category);
                ViewBag.IsVertical = new SelectList(IsVerticalLst, IsVertical);
                ViewBag.Expired = new SelectList(ExpiredLst, Expired);

                var postads = db.PostAds.Include(p => p.PostCategory);
                if (!String.IsNullOrEmpty(Search))
                    postads = postads.Where(a => a.ImageURL.Contains(Search)
                                              || a.ImageLink.Contains(Search)
                                           );
                if (!String.IsNullOrEmpty(Category) && Category != "All")
                    postads = postads.Where(a => a.PostCategory.CategoryName == Category);

                if (!String.IsNullOrEmpty(IsVertical) && IsVertical != "All")
                {
                    postads = postads.Where(a => a.isVertical == ((IsVertical == "Ngang") ? true : false));
                }

                if (!String.IsNullOrEmpty(Expired) && Expired != "All")
                {
                    if (Expired == "Còn Hạn")
                        postads = postads.Where(a => a.ExpireDate.Value < DateTime.Now);
                    else
                        postads = postads.Where(a => a.ExpireDate.Value > DateTime.Now);
                }

                ViewBag.searchstring = Search;
                return View(postads.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /PostAd/Details/5

        public ActionResult Details(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                PostAd postad = db.PostAds.Find(id);
                if (postad == null)
                {
                    return HttpNotFound();
                }
                return View(postad);
            }
            else return HttpNotFound();
        }

        //
        // GET: /PostAd/Create

        public ActionResult Create()
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                ViewBag.PostId = new SelectList(db.Posts, "PostId", "Title");
                return View();
            }
            else return HttpNotFound();
        }

        //
        // POST: /PostAd/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostAd postad)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                if (ModelState.IsValid)
                {
                    db.PostAds.Add(postad);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(postad);
            }
            else return HttpNotFound();
        }

        //
        // GET: /PostAd/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                PostAd postad = db.PostAds.Find(id);
                if (postad == null)
                {
                    return HttpNotFound();
                }
                return View(postad);
            }
            else return HttpNotFound();
        }

        //
        // POST: /PostAd/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PostAd postad)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(postad).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(postad);
            }
            else return HttpNotFound();
        }

        //
        // GET: /PostAd/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                PostAd postad = db.PostAds.Find(id);
                if (postad == null)
                {
                    return HttpNotFound();
                }
                return View(postad);
            }
            else return HttpNotFound();
        }

        //
        // POST: /PostAd/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                PostAd postad = db.PostAds.Find(id);
                FileController a = new FileController();
                a.Delete1(postad.ImageURL);
                db.PostAds.Remove(postad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}