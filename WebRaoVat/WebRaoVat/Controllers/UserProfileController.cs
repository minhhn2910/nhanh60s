﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebRaoVat.Filters;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class UserProfileController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /UserProfile/5

        public ActionResult Index()
        {
            int CurrentId = WebSecurity.CurrentUserId; // >=0 if exist; -1 if anonymous user

            return RedirectToAction("Details");
            
        }

        //
        // GET: /UserProfile/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = -1, int page = 1, int PageSize = 5)
        {
            if(id<0)
                id = WebSecurity.CurrentUserId;
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            db.Entry(userprofile).State = EntityState.Modified;
            userprofile.HitCount = ((userprofile.HitCount != null) ? userprofile.HitCount + 1 : 1);
            db.SaveChanges();

            ViewBag.CurrentPage = page;
            ViewBag.PageSize = PageSize;
            ViewBag.id = id;
            ViewBag.TotalPages = (int)Math.Ceiling((double)userprofile.Posts.Count() / PageSize);
            userprofile.Posts = userprofile.Posts.Skip((page - 1) * PageSize).Take(PageSize).ToList();

            var Ads = db.UserAds.Where(p => p.UserId == id);
            Ads = Ads.Where(p => p.Approved == true);
            ViewBag.UserAds = "none";
            if (Ads != null)
            {
                ViewBag.UserAds = Ads.OrderBy(p => p.ImageOdr).ToList();
            }

            return View(userprofile);
        }

        //User profile is created in AccountController, no need for recreating here
        //
        // GET: /UserProfile/Create
/*
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserProfile/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.UserProfiles.Add(userprofile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userprofile);
        }
*/
        // upgrade user, can be done with admin or user
        // GET: /UserProfile/Upgrade/
        public ActionResult Upgrade(int id =-1)
        {
            if(id<0)
                id = WebSecurity.CurrentUserId;
            
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
                return HttpNotFound();
            userprofile.UserType = (int) UserType.DoanhNghiep; //0: canhan, 1 doanh nghiep
            db.Entry(userprofile).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Edit");
        }



        //
        // GET: /UserProfile/Edit/

        public ActionResult Edit()
        {
            int id = WebSecurity.CurrentUserId;
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
        /*      if (userprofile.UserType == UserType.KhachHangCaNhan.ToString())
                  ViewBag.UserType=userprofile.UserType
              else
                  return DoanhNghiep(userprofile);*/
              List<string> ImagesCollection = new List<string>();
              ImagesCollection.Add(userprofile.LogoImage);
              ImagesCollection.Add(userprofile.BannerImage);
              ViewBag.Images = ImagesCollection.ToArray();
              return View(userprofile);
        }

        //
        // POST: /UserProfile/Edit/

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserProfile userprofile)
        {
            int CurrentId = WebSecurity.CurrentUserId;
            if (CurrentId != userprofile.UserId)
               return HttpNotFound();

            string[] images_links = null;
            if (Request["images"] != null)
                images_links = Request["images"].ToString().Split(':');
/* Check valid by javascript client side. better than this one
            if (ModelState.IsValid)
            {
            */
            if (images_links.Length ==2)
            {
                if (!String.IsNullOrEmpty(images_links[0]))
                    userprofile.LogoImage = images_links[0];
                else
                    userprofile.LogoImage = "NoImage.jpg";

                if (!String.IsNullOrEmpty(images_links[1]))
                    userprofile.BannerImage = images_links[1];
                else
                    userprofile.BannerImage = "NoImage.jpg";
            }
            else if (images_links.Length == 1)
            {
                if (!String.IsNullOrEmpty(images_links[0]))
                    userprofile.LogoImage = images_links[0];
                else
                    userprofile.LogoImage = "NoImage.jpg";
            }

                db.Entry(userprofile).State = EntityState.Modified;
                db.SaveChanges();
              //  return HttpNotFound();
                return RedirectToAction("Details");
  /*          }

            return View(userprofile);*/
        }
/*
        //
        // GET: /UserProfile/Delete/5

        public ActionResult Delete(int id = 0)
        {
            id = WebSecurity.CurrentUserId;

            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /UserProfile/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        */
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public enum UserType
        {
            KhachHangCaNhan,
            DoanhNghiep
        }
    }
}