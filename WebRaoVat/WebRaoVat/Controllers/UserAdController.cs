﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class UserAdController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /UserAd/

        public ActionResult Index()
        {
            var userads = db.UserAds.Where( u => u.UserId == WebSecurity.CurrentUserId).Include(u => u.UserProfile);
            return View(userads.ToList());
        }

        //
        // GET: /UserAd/UserAdManager

        public ActionResult UserAdManager(string Search, string Approved)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var ApprovedLst = new List<string>();
                ApprovedLst.Add("All");
                ApprovedLst.Add("Chưa kiểm duyệt");
                ApprovedLst.Add("Đã kiểm duyệt");

                ViewBag.Approved = new SelectList(ApprovedLst, Approved);
                var userads = db.UserAds.Include(u => u.UserProfile);
                if (!String.IsNullOrEmpty(Search))
                    userads = userads.Where(a => a.ImageURL.Contains(Search)
                                            || a.ImageLink.Contains(Search)
                                           );
                if (!String.IsNullOrEmpty(Approved) && Approved != "All")
                {
                    userads = userads.Where(a => a.Approved == ((Approved == "Đã kiểm duyệt") ? true : false));
                }

                ViewBag.searchstring = Search;
                return View(userads.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserAd/Details/5

        public ActionResult Details(int id = 0)
        {
            UserAd userad = db.UserAds.Find(id);
            if (userad == null)
            {
                return HttpNotFound();
            }
            return View(userad);
        }

        //
        // GET: /UserAd/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.UserProfiles.Where(u => u.UserId == WebSecurity.CurrentUserId), "UserId", "UserName");
            return View();
        }

        //
        // POST: /UserAd/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserAd userad)
        {
            string[] images_links = null;
            if (Request["images"] != null)
                images_links = Request["images"].ToString().Split(':');
//               if (ModelState.IsValid)
//                {
                if (images_links.Length == 1)
                {
                    if (!String.IsNullOrEmpty(images_links[0]))
                        userad.ImageURL = images_links[0];
                    else
                        userad.ImageURL = "NoImage.jpg";
                }
                else
                    userad.ImageURL = "NoImage.jpg";
                userad.UserId = WebSecurity.CurrentUserId;
                db.UserAds.Add(userad);
                db.SaveChanges();
                return RedirectToAction("Index");
//            }
//
//            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", userad.UserId);
//            return View(userad);
        }

        //
        // GET: /UserAd/Edit/5

        public ActionResult Edit(int id = 0)
        {
            UserAd userad = db.UserAds.Find(id);
            ViewBag.EditableApproved = 0;
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", userad.UserId);
                ViewBag.EditableApproved = 1;
                return View(userad);
            }
            if (userad == null || userad.UserId != WebSecurity.CurrentUserId)
            {
                return HttpNotFound();
            }
            
            ViewBag.UserId = new SelectList(db.UserProfiles.Where(u => u.UserId == WebSecurity.CurrentUserId), "UserId", "UserName", userad.UserId);
            return View(userad);
        }

        //
        // POST: /UserAd/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserAd userad)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) == null)
                return HttpNotFound();
            if (userad == null || (userad.UserId != WebSecurity.CurrentUserId && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100))
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Entry(userad).State = EntityState.Modified;
                db.SaveChanges();
                if (db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
                    return RedirectToAction("UserAdManager");
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.UserProfiles.Where(u => u.UserId == WebSecurity.CurrentUserId), "UserId", "UserName", userad.UserId);
            return View(userad);
        }

        //
        // GET: /UserAd/Delete/5

        public ActionResult Delete(int id = 0)
        {
            UserAd userad = db.UserAds.Find(id);
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) == null)
                return HttpNotFound();
            if (userad == null || (userad.UserId != WebSecurity.CurrentUserId && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100))
            {
                return HttpNotFound();
            }
            return View(userad);
        }

        //
        // POST: /UserAd/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserAd userad = db.UserAds.Find(id);
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) == null)
                return HttpNotFound();
            if (userad == null || (userad.UserId != WebSecurity.CurrentUserId && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100))
            {
                return HttpNotFound();
            }
            FileController a = new FileController();
            a.Delete1(userad.ImageURL);
            db.UserAds.Remove(userad);            
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}