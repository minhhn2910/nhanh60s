﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    public class PostCategoryController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /PostCategory/

        public ActionResult Index()
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var postcategories = db.PostCategories.Include(p => p.UserProfile);
                return View(postcategories.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /PostCategory/Details/5
       
        public ActionResult Details(int CatId = 0, int page=1, int PageSize=10,int SortBy=2, bool IsAsc=false, string Search=null)
        {
            if (CatId==0)
                return RedirectToAction("Index", "Post", new { Search = Search });
            // tim kiem truoc khi sort, gioi han so data
            var PostList = db.PostCategories.Find(CatId);
            if (PostList == null)
            {
                return HttpNotFound();
            }
            IEnumerable<Post> PostListFiltered;
            try
            {
                PostListFiltered = PostList.Posts.Where(
                                                                  p => Search == null
                                                                    || p.Title.Contains(Search)
                                                                    || p.ShortDescription.Contains(Search)
                                                                    || p.Detail.Contains(Search)
                                                                    || p.TitleAlias.Contains(Search));
                if (PostListFiltered != null)
                {
                    PostListFiltered = PostListFiltered.Where(p => p.Approved == true);
                }
            }
            catch (System.NullReferenceException e) //exeption xay ra khi 1 trong cac field == null 
            {       
                    return HttpNotFound();
            };

            //khong tim thay category, return 404

            
            // xu ly sap xep bai post 
            switch (SortBy)
            {
                case 1:
                    PostListFiltered = IsAsc ? PostListFiltered.OrderBy(p => p.Title) : PostListFiltered.OrderByDescending(p => p.Title);
                    break;
                case 2:
                    PostListFiltered = IsAsc ? PostListFiltered.OrderBy(p => p.Created) : PostListFiltered.OrderByDescending(p => p.Created);
                    break;
                case 3:
                    PostListFiltered = IsAsc ? PostListFiltered.OrderBy(p => p.HitCount) : PostListFiltered.OrderByDescending(p => p.HitCount);
                    break;
                case 4:
                    PostListFiltered = IsAsc ? PostListFiltered.OrderBy(p => p.Price) : PostListFiltered.OrderByDescending(p => p.Price);
                    break;
                default:
                    PostListFiltered = IsAsc ? PostListFiltered.OrderBy(p => p.PostId) : PostListFiltered.OrderByDescending(p => p.PostId);
                    break;
            }
            //paging , numitem = pagesize
            

            ViewBag.CatId = CatId;
            ViewBag.CurrentPage = page;
            ViewBag.PageSize = PageSize;
            ViewBag.TotalPages = (int)Math.Ceiling((double)PostListFiltered.Count() / PageSize);
            ViewBag.SortBy = SortBy;
            ViewBag.IsAsc = IsAsc;
            ViewBag.Search = Search;
            ViewBag.CategoryName = PostList.CategoryName;
            PostListFiltered = PostListFiltered.Skip((page - 1) * PageSize).Take(PageSize).ToList();

            var Ads = db.CategoryAds.Where(p => p.CategoryId == CatId);
            Ads = Ads.Where(p => p.ExpireDate > DateTime.Now);
            ViewBag.CategoryAds = "none";
            if (Ads != null)
            {
                ViewBag.CategoryAds = Ads.OrderBy(p => p.ImageOdr).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }

            return View(PostListFiltered.ToList());
        }

/*
        //
        // GET: /PostCategory/Create

        public ActionResult Create()
        {
            ViewBag.ModUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /PostCategory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostCategory postcategory)
        {
            if (ModelState.IsValid)
            {
                db.PostCategories.Add(postcategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ModUserId = new SelectList(db.UserProfiles, "UserId", "UserName", postcategory.ModUserId);
            return View(postcategory);
        }

        //
        // GET: /PostCategory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PostCategory postcategory = db.PostCategories.Find(id);
            if (postcategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModUserId = new SelectList(db.UserProfiles, "UserId", "UserName", postcategory.ModUserId);
            return View(postcategory);
        }

        //
        // POST: /PostCategory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PostCategory postcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postcategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ModUserId = new SelectList(db.UserProfiles, "UserId", "UserName", postcategory.ModUserId);
            return View(postcategory);
        }

        //
        // GET: /PostCategory/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PostCategory postcategory = db.PostCategories.Find(id);
            if (postcategory == null)
            {
                return HttpNotFound();
            }
            return View(postcategory);
        }

        //
        // POST: /PostCategory/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PostCategory postcategory = db.PostCategories.Find(id);
            db.PostCategories.Remove(postcategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
*/
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}