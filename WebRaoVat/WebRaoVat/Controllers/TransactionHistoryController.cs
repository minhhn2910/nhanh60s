﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class TransactionHistoryController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /TransactionHistory/

        public ActionResult Index(string Search)
        {
            var transactionhistories = db.TransactionHistories.Where(t => t.UserWallet.UserId == WebSecurity.CurrentUserId).Include(t => t.UserWallet);
            if (!String.IsNullOrEmpty(Search))
                transactionhistories = transactionhistories.Where(a => a.TransactionContent.Contains(Search));
            ViewBag.searchstring = Search;
            return View(transactionhistories.ToList());
        }

        //
        // GET: /TransactionHistory/TransactionManager

        public ActionResult TransactionManager(string Search, string UserName)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var transactionhistories = db.TransactionHistories.Include(t => t.UserWallet);
                if (!String.IsNullOrEmpty(UserName))
                    transactionhistories = transactionhistories.Where(a => a.TransactionContent.Contains(UserName));
                if (!String.IsNullOrEmpty(Search))
                    transactionhistories = transactionhistories.Where(a => a.TransactionContent.Contains(Search));

                ViewBag.UserName = UserName;
                ViewBag.searchstring = Search;
                return View(transactionhistories.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /TransactionHistory/Details/5
/*
        public ActionResult Details(int id = 0)
        {
            TransactionHistory transactionhistory = db.TransactionHistories.Find(id);
            if (transactionhistory == null)
            {
                return HttpNotFound();
            }
            return View(transactionhistory);
        }
*/
        //
        // GET: /TransactionHistory/Create

        public ActionResult Create()
        {
            ViewBag.UserWalletId = new SelectList(db.UserWallets, "UserWalletId", "UserWalletId");
            return View();
        }

        //
        // POST: /TransactionHistory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TransactionHistory transactionhistory)
        {
            if (ModelState.IsValid)
            {
                db.TransactionHistories.Add(transactionhistory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserWalletId = new SelectList(db.UserWallets, "UserWalletId", "UserWalletId", transactionhistory.UserWalletId);
            return View(transactionhistory);
        }

        //
        // GET: /TransactionHistory/Edit/5
/*
        public ActionResult Edit(int id = 0)
        {
            TransactionHistory transactionhistory = db.TransactionHistories.Find(id);
            if (transactionhistory == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserWalletId = new SelectList(db.UserWallets, "UserWalletId", "UserWalletId", transactionhistory.UserWalletId);
            return View(transactionhistory);
        }

        //
        // POST: /TransactionHistory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TransactionHistory transactionhistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transactionhistory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserWalletId = new SelectList(db.UserWallets, "UserWalletId", "UserWalletId", transactionhistory.UserWalletId);
            return View(transactionhistory);
        }

        //
        // GET: /TransactionHistory/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TransactionHistory transactionhistory = db.TransactionHistories.Find(id);
            if (transactionhistory == null)
            {
                return HttpNotFound();
            }
            return View(transactionhistory);
        }

        //
        // POST: /TransactionHistory/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TransactionHistory transactionhistory = db.TransactionHistories.Find(id);
            db.TransactionHistories.Remove(transactionhistory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
*/
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}