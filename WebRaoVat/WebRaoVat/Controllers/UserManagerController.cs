﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using System.Web.Security;


namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class UserManagerController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /UserManager/

        public ActionResult Index(string Search, string BusinessExpired, string BanExpired)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                var ExpiredLst = new List<string>();
                ExpiredLst.Add("All");
                ExpiredLst.Add("Not Yet");
                ExpiredLst.Add("Expired");

                ViewBag.BusinessExpired = new SelectList(ExpiredLst, BusinessExpired);
                ViewBag.BanExpired = new SelectList(ExpiredLst, BanExpired);

                IEnumerable<UserProfile> user = db.UserProfiles;
                if (!String.IsNullOrEmpty(Search))
                    user = user.Where( a => a.Address.Contains(Search)
                                            || a.BannerImage.Contains(Search)
                                            || a.Email.Contains(Search)
                                            || a.Fax.Contains(Search)
                                            || a.FullName.Contains(Search)
                                            || a.HotLine.Contains(Search)
                                            || a.LogoImage.Contains(Search)
                                            || a.Phone.Contains(Search)
                                            || a.Skype.Contains(Search)
                                            || a.Slogan.Contains(Search)
                                            || a.UserName.Contains(Search)
                                            || a.Website.Contains(Search)
                                            || a.Yahoo.Contains(Search)
                                           );

                if (!String.IsNullOrEmpty(BusinessExpired) && BusinessExpired != "All")
                {
                    if (BusinessExpired == "Not Yet")
                        user = user.Where(a => a.BusinessExpireDate.Value < DateTime.Now);
                    else
                        user = user.Where(a => a.BusinessExpireDate.Value > DateTime.Now);
                }

                if (!String.IsNullOrEmpty(BanExpired) && BanExpired != "All")
                {
                    if (BanExpired == "Not Yet")
                        user = user.Where(a => a.UserBanExpireDate.Value < DateTime.Now);
                    else
                        user = user.Where(a => a.UserBanExpireDate.Value > DateTime.Now);
                }
                return View(user.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserManager/Details/5

        public ActionResult Details(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                UserProfile userprofile = db.UserProfiles.Find(id);
                if (userprofile == null)
                {
                    return HttpNotFound();
                }
                return View(userprofile);
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserManager/Create

        public ActionResult Create()
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                return View();
            }
            else return HttpNotFound();
        }

        //
        // POST: /UserManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserProfile userprofile)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                if (ModelState.IsValid)
                {
                    db.UserProfiles.Add(userprofile);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(userprofile);
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                UserProfile userprofile = db.UserProfiles.Find(id);
                if (userprofile == null)
                {
                    return HttpNotFound();
                }
                return View(userprofile);
            }
            else return HttpNotFound();
        }

        //
        // POST: /UserManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserProfile userprofile)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(userprofile).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(userprofile);
            }
            else return HttpNotFound();
        }

        //
        // GET: /UserManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                UserProfile userprofile = db.UserProfiles.Find(id);
                if (userprofile == null)
                {
                    return HttpNotFound();
                }
                return View(userprofile);
            }
            else return HttpNotFound();
        }

        //
        // POST: /UserManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 200)
            {
                UserProfile userprofile = db.UserProfiles.Find(id);
                db.UserProfiles.Remove(userprofile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}