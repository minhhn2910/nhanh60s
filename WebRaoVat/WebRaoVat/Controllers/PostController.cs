﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using WebRaoVat.Models;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class PostController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index(int CatId = 0,int page = 1, int PageSize = 5, int SortBy = 1, bool IsAsc = true, string Search = null)
        {
            if (CatId != 0) // user want to view specific category's post
                return RedirectToAction("PostCategory", "Details", new { CatId = CatId });

          /*  var posts = db.Posts.Include(p => p.PostCategory).Include(p => p.UserProfile).Include(p => p.Zone);*/
            //tim kiem post
            var posts = db.Posts.Where(
                                    p => Search == null
                                    || p.Title.Contains(Search)
                                    || p.ShortDescription.Contains(Search)
                                    || p.Detail.Contains(Search)
                                    || p.TitleAlias.Contains(Search));
            posts = posts.Where(p => p.Approved == true);
            IEnumerable<Post> postlist;
            switch (SortBy)
            {
                case 1:
                    postlist = IsAsc ? posts.OrderBy(p => p.Title) : posts.OrderByDescending(p => p.Title);
                    break;
                case 2:
                    postlist = IsAsc ? posts.OrderBy(p => p.Created) : posts.OrderByDescending(p => p.Created);
                    break;
                case 3:
                    postlist = IsAsc ? posts.OrderBy(p => p.HitCount) : posts.OrderByDescending(p => p.HitCount);
                    break;
                case 4:
                    postlist = IsAsc ? posts.OrderBy(p => p.Price) : posts.OrderByDescending(p => p.Price);
                    break;
                default:
                    postlist = IsAsc ? posts.OrderBy(p => p.PostId) : posts.OrderByDescending(p => p.PostId);
                    break;
            }
            ViewBag.CurrentPage = page;
            ViewBag.PageSize = PageSize;
            ViewBag.TotalPages = (int)Math.Ceiling((double)postlist.Count() / PageSize);
            ViewBag.SortBy = SortBy;
            ViewBag.IsAsc = IsAsc;
            ViewBag.Search = Search;
            postlist = postlist.Skip((page - 1) * PageSize).Take(PageSize).ToList();
            return View(postlist.ToList());
        }

        //
        // GET: /Post/PostManager
        public ActionResult PostManager(string Search, string UserName, string Category, string Zone, string IsActive, string PostType, string Approved, string Expired)
        {
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                var CategoryLst = new List<string>();
                var ZoneLst = new List<string>();
                var PostTypeLst = new List<string>();
                var IsActiveLst = new List<string>();
                var ExpiredLst = new List<string>();
                var ApprovedLst = new List<string>();

                var CategoryQry = from d in db.PostCategories
                                  select d.CategoryName;
                var ZoneQry = from d in db.Zones
                                  select d.ZoneName;

                PostTypeLst.Add("All");
                PostTypeLst.Add("Tin Vip");
                PostTypeLst.Add("Tin Thuong");

                ExpiredLst.Add("All");
                ExpiredLst.Add("Hết Hạn");
                ExpiredLst.Add("Còn Hạn");

                CategoryLst.Add("All");
                CategoryLst.AddRange(CategoryQry.Distinct());
                ZoneLst.Add("All");
                ZoneLst.AddRange(ZoneQry.Distinct());

                ApprovedLst.Add("All");
                ApprovedLst.Add("Chưa kiểm duyệt");
                ApprovedLst.Add("Đã kiểm duyệt");

                IsActiveLst.Add("All");
                IsActiveLst.Add("Đang Hiển Thị");
                IsActiveLst.Add("Đang Ẩn");
            
                ViewBag.Category = new SelectList(CategoryLst, Category);
                ViewBag.Zone = new SelectList(ZoneLst, Zone);
                ViewBag.PostType = new SelectList(PostTypeLst, PostType);
                ViewBag.IsActive = new SelectList(IsActiveLst, IsActive);
                ViewBag.Expired = new SelectList(ExpiredLst, Expired);
                ViewBag.Approved = new SelectList(ApprovedLst, Approved);
                ViewBag.searchstring = Search;
                ViewBag.UserName = UserName;

                var post = db.Posts.Include(p => p.PostCategory);
                if (!String.IsNullOrEmpty(UserName))
                    post = post.Where(a => a.UserProfile.UserName.Contains(UserName));
                if (!String.IsNullOrEmpty(Search))
                    post = post.Where(a => a.Detail.Contains(Search)
                                        || a.ShortDescription.Contains(Search)
                                        || a.Title.Contains(Search)
                                        || a.TitleAlias.Contains(Search)
                                     );
                if (!String.IsNullOrEmpty(Category) && Category != "All")
                    post = post.Where(a => a.PostCategory.CategoryName == Category);
                if (!String.IsNullOrEmpty(Zone) && Zone != "All")
                    post = post.Where(a => a.Zone.ZoneName == Zone);
                if (!String.IsNullOrEmpty(Approved) && Approved != "All")
                {
                    post = post.Where(a => a.Approved == ((Approved == "Đã kiểm duyệt") ? true : false));
                }
                if (!String.IsNullOrEmpty(PostType) && PostType != "All")
                {
                    if (PostType == "Tin Vip")
                        post = post.Where(a => a.PostType == 1);
                    else
                        post = post.Where(a => a.PostType == 0);
                }
                if (!String.IsNullOrEmpty(IsActive) && IsActive != "All")
                {
                    post = post.Where(a => a.Active == ((IsActive == "Đang Hiển Thị") ? true : false));
                }

                if (!String.IsNullOrEmpty(Expired) && Expired != "All")
                {
                    if (Expired == "Còn Hạn")
                        post = post.Where(a => a.ExpireDate.Value < DateTime.Now);
                    else
                        post = post.Where(a => a.ExpireDate.Value > DateTime.Now);
                }

                return View(post.ToList());
            }
            else return HttpNotFound();
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            
            Post post = db.Posts.Find(id);
            
            if (post == null)
            {
                return HttpNotFound();
            }
            if ((WebSecurity.CurrentUserId == post.UserId) || (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100))
                ViewBag.Editable = 1;
            else
                ViewBag.Editable = 0;
            db.Entry(post).State = EntityState.Modified;
            post.HitCount = ((post.HitCount != null) ? post.HitCount + 1 : 1);
            db.SaveChanges();

            ViewBag.Bookmarked = 0;
            if (WebSecurity.CurrentUserId > 0)
            {
                var temp = db.Bookmarks.Where(u => u.UserId == WebSecurity.CurrentUserId);
                if (temp.Where(u => u.PostId == id).Count() != 0)
                {
                    ViewBag.Bookmarked = 1;
                    ViewBag.BookmarkId = temp.Where(u => u.PostId == id).First().BookmarkId;
                }
            }
            var Ads = db.PostAds.Where(p => p.CategoryId == post.CategoryId);
            Ads = Ads.Where(p => p.ExpireDate > DateTime.Now);
            ViewBag.PostAds = null;
            if (Ads != null)
            {
                ViewBag.PostAds = Ads.OrderBy(p => p.ImageOdr).ToList();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName");
            ViewBag.UserId = WebSecurity.CurrentUserId;
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName");
            ViewBag.ShortDescription = "";
            ViewBag.Detail = "";
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post)
        {
          string[] images_links=null;
          if (Request["images"] != null)
             images_links = Request["images"].ToString().Split(':');

           if (ModelState.IsValid)
            {
                //first link is thumbnail;
                if (!String.IsNullOrEmpty(images_links[0]))
                    post.Thumbnail = images_links[0];
                else
                    post.Thumbnail = "NoImage.jpg";
                if (images_links.Length == 1 || String.IsNullOrEmpty(images_links[1]))
                {
                    images_links = new string[2];
                    images_links[1] = "NoImage.jpg";
                }                
                post.TitleAlias = "sample-alias"; //processing alias here
                post.Created = System.DateTime.Now;
                post.Active = true;
                post.Approved = false;
                post.ExpireDate = DateTime.Now.AddMonths(1);
                post.HitCount = 1;
                post.Detail = Server.HtmlEncode(post.Detail);
                if ((db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100) && post.CategoryId == 14)
                    post.CategoryId = 12;
                db.Posts.Add(post);

                        for (int i = 1; i < images_links.Length; i++) //skip first image
                        {
                            if (!String.IsNullOrEmpty(images_links[i]))
                            {
                                PostImage post_image;
                                post_image = new PostImage();
                                post_image.ImageURL = images_links[i].ToString();
                                post_image.PostId = post.PostId;
                                db.PostImages.Add(post_image);
                            }
                        }
                    
                    db.SaveChanges();
                return RedirectToAction("Details", new { id = post.PostId });
           }
           ViewBag.ShortDescription = post.ShortDescription;
           ViewBag.Detail = post.Detail;
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", post.CategoryId);
            ViewBag.UserId = ViewBag.UserId = WebSecurity.CurrentUserId;
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName", post.ZoneId);
            if (images_links.Length>0 && images_links[0]!="")
                ViewBag.Images = images_links;
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) == null)
                return HttpNotFound();
            if (post == null || (WebSecurity.CurrentUserId != post.UserId && (db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100))) //only user authorized can edit
            {
                return HttpNotFound();
            }
            ViewBag.EditableApproved = 0;
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100)
            {
                ViewBag.EditableApproved = 1;
            }
/*            var PostTypeLst = new List<string>();
            PostTypeLst.Add("Tin Vip");
            PostTypeLst.Add("Tin Thường");

            ViewBag.PostType = new SelectList(PostTypeLst, "Tin Vip");
 */
            ViewBag.PostType = post.PostType;

            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", post.CategoryId);
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName", post.ZoneId);
            List<string> ImagesCollection = new List<string>();
            ImagesCollection.Add(post.Thumbnail);
            ImagesCollection.AddRange(post.PostImages.Select(a => a.ImageURL).ToList());
            ViewBag.Images = ImagesCollection.ToArray();
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {       
            string[] images_links = null;
            if (Request["images"] != null)
                images_links = Request["images"].ToString().Split(':');
            if (db.UserProfiles.Find(WebSecurity.CurrentUserId) == null)
                return HttpNotFound();
            if (post.UserId != WebSecurity.CurrentUserId && (db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100))
                return HttpNotFound();
//            if (ModelState.IsValid)
//            {
                //first link is thumbnail;
                if (!String.IsNullOrEmpty(images_links[0]))
                    post.Thumbnail = images_links[0];
                post.TitleAlias = "sample-alias"; //processing alias here
                if ((db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType <= 100) && post.CategoryId == 14)
                    post.CategoryId = 12;
                db.Entry(post).State = EntityState.Modified;

                IEnumerable<PostImage> post_images = db.PostImages.Where(t => t.PostId == post.PostId);
                foreach (PostImage image in post_images)
                {
                    db.PostImages.Remove(image);
                }

                for (int i = 1; i < images_links.Length; i++) //skip first image
                {
                    if (!String.IsNullOrEmpty(images_links[i]))
                    {
                        PostImage post_image;
                        post_image = new PostImage();
                        post_image.ImageURL = images_links[i].ToString();
                        post_image.PostId = post.PostId;
                        db.PostImages.Add(post_image);
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Details", new { id = post.PostId });
    //        }
/*
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", post.CategoryId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserId);
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName", post.ZoneId);

            if (images_links != null)
                ViewBag.Images = images_links;
            
            return View(post);
 * */
        }

        //
        // GET: /Post/Touch/5

        public ActionResult Touch(int id = 0)
        {
            if (db.Posts.Find(id) != null && db.Posts.Find(id).UserId == WebSecurity.CurrentUserId)
            {
                if (db.Posts.Find(id).Created == null || DateTime.Now > db.Posts.Find(id).Created.Value.AddHours(12))
                {
                    db.Posts.Find(id).Created = DateTime.Now;
                    db.Entry(db.Posts.Find(id)).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    ViewBag.link_id = id;
                    return View();
                }
            }
            return HttpNotFound();// RedirectToAction("Details", new { id = id });
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if ((WebSecurity.CurrentUserId == post.UserId) || (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100))
            {
                return View(post);
            }
            return HttpNotFound();
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if ((WebSecurity.CurrentUserId == post.UserId) || (db.UserProfiles.Find(WebSecurity.CurrentUserId) != null && db.UserProfiles.Find(WebSecurity.CurrentUserId).UserType > 100))
            {
                FileController a = new FileController();
                a.Delete1(post.Thumbnail);
                IEnumerable<PostImage> post_images = db.PostImages.Where(t => t.PostId == post.PostId);
                try
                {
                    foreach (PostImage image in post_images)
                    {
                        //db.PostImages.Remove(image);
                        a.Delete1(image.ImageURL);
                        //db.SaveChanges();
                    }
                }
                catch
                {

                }
               /* IEnumerable<Bookmark> bookmarks = db.Bookmarks.Where(t => t.PostId == post.PostId);
                try
                {
                    foreach (Bookmark bookmark in bookmarks)
                    {
                        //db.Entry(bookmark).State = EntityState.Deleted;
                        db.Bookmarks.Remove(bookmark);
                        db.SaveChanges();
                    }
                }
                catch
                {

                }*/
                db.Posts.Remove(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}