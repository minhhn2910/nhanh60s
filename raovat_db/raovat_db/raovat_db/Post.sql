﻿CREATE TABLE [dbo].[Post]
(
	[PostId] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [UserId] INT NULL, 
    [Created] DATETIME NULL DEFAULT 22/2/2222, 
    [Title] NVARCHAR(500) NULL, 
	[TitleAlias] NVARCHAR(500) NULL, 
	[CategoryId] INT NOT NULL, 
    [ShortDescription] NVARCHAR(MAX) NULL, 
    [Detail] NVARCHAR(MAX) NULL, 
    [Price] NVARCHAR(50) NULL, 
    [ZoneId] INT NULL, 
    [Active] BIT NULL,
	[PostType] TINYINT NULL, 
    [HitCount] INT NULL, 
	[Thumbnail] NVARCHAR(500) NULL, 
    CONSTRAINT [FK_Post_User] FOREIGN KEY ([UserId]) REFERENCES [UserProfile]([UserId]) ON DELETE CASCADE, 
    CONSTRAINT [FK_Post_Category] FOREIGN KEY ([CategoryId]) REFERENCES [PostCategory]([CategoryId]), 
    CONSTRAINT [FK_Post_Zone] FOREIGN KEY ([ZoneId]) REFERENCES [Zone]([ZoneId])
)
