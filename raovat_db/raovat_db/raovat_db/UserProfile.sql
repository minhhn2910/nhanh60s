﻿CREATE TABLE [dbo].[UserProfile]
(
	[UserId] INT  IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [UserName] NVARCHAR(50) NULL, 
	[FullName] NVARCHAR(500) NULL, 
    [UserType] INT NULL, 
    [BannerImage] NVARCHAR(500) NULL, 
    [Address] NVARCHAR(500) NULL, 
    [Phone] NVARCHAR(50) NULL, 
    [Fax] NVARCHAR(50) NULL, 
    [Website] NVARCHAR(50) NULL, 
    [Email] NVARCHAR(50) NULL, 
    [Review] TINYINT NULL, 
    [Rating] TINYINT NULL, 
    [Yahoo] NVARCHAR(50) NULL, 
    [Skype] NVARCHAR(50) NULL, 
    [HotLine] NVARCHAR(50) NULL, 
    [HitCount] INT NULL, 
    [NumPosts] INT NULL, 
    [Slogan] NVARCHAR(500) NULL, 
    [LogoImage] NVARCHAR(500) NULL, 
    [CustomAds] NVARCHAR(500) NULL
)
